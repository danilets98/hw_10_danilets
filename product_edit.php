<?php
/**
 * Created by PhpStorm.
 * User: OverLord
 * Date: 14.12.2017
 * Time: 21:48
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'DB/connect_db.php';
if(!empty($_GET['id'])) {
    $id= $_GET['id'];
    $sql = "SELECT * FROM product WHERE id = " . $id;
    $result = $pdoDB->query($sql);
    $result_obj = $result->fetchObject();



}?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2>Edit product</h2>
<!-- формы для редактирования-->
<form action="product_update.php" method="POST">
    <label for="product_title">product title:</label><br>
    <textarea name="product_title" id="product_title" cols="30" rows="10"><?= $result_obj->title?></textarea><br>

    <label for="product_description">product description:</label><br>
    <textarea name="product_description" id="product_description" cols="30" rows="10"><?= $result_obj->description?></textarea><br>

    <label for="product_price">product price:</label><br>
    <textarea name="product_price" id="product_price" cols="30" rows="10"><?= $result_obj->price?></textarea><br>

    <label for="product_type">product type:</label><br>
    <textarea name="product_type" id="product_type" cols="30" rows="10"><?= $result_obj->type?></textarea><br>

    <input type="hidden" name="product_id" value="<?=$result_obj->id?>">
    <button type="submit">Update</button>
</form>


</body>
</body>
</html>