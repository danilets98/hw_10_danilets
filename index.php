<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'DB/connect_db.php';

try{
    $sql="SELECT * FROM product";
    $result = $pdoDB->query($sql);
}catch(PDOException $e){
    die('Ошибка при извлечении из Базы данных'.$e->getMessage());
}
$resultArray= $result->fetchALL();

    ?>

<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<?php foreach ( $resultArray as $product): ?>
    <!-- Вывод названий и слыки на просмотр и редактирование и удаление-->
    <div>
        <a href="product_show.php?id=<?=$product['id'];?>">
            <?=$product['title']?>
        </a><br>
        <a href="product_edit.php?id=<?=$product['id'];?>">Edit</a>

        <form action="product_delete.php" method="post">

            <input type="hidden" name="product_id" value="<?=$product['id']?>">
            <button type="submit" >DELETE</button>
        </form>
    </div>
<?php endforeach; ?>
<!-- Формы для добавление товаров-->
<form action="product_add.php" method="POST">
    <label for="product_title">product title:</label><br>
    <textarea name="product_title" id="product_title" cols="30" rows="10"></textarea><br>

    <label for="product_description">product description:</label><br>
    <textarea name="product_description" id="product_description" cols="30" rows="10"></textarea><br>

    <label for="product_price">product price:</label><br>
    <textarea name="product_price" id="product_price" cols="30" rows="10"></textarea><br>

    <label for="product_type">product type:</label><br>
    <textarea name="product_type" id="product_type" cols="30" rows="10"></textarea><br>


    <button type="submit">Save</button>
</form>


</body>
</html>
